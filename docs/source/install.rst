How to install Brebis
=====================

* You need at least Python 3.4.
* Untar the tarball and go to the source directory with the following commands:::

    $ tar zxvf brebis-0.10.tar.gz
    $ cd brebis

* Next, to install Brebis on your computer, type the following command with the root user:::

    # python3.4 setup.py install
    # # or
    # python3.4 setup.py install --install-scripts=/usr/bin

