Documentation for the Brebis project
====================================

You'll find below anything you need to install, configure or run Brebis.

Guide
=====

.. toctree::
   :maxdepth: 2

   install
   configure
   use
   unsupported
   license
   authors


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

